/*
   Ejercicio 72
    Dada la variable
    let puntuación = 65;
    Escriba una instrucción if en JavaScript que muestre un carácter en la consola (que represente una calificación) según el valor almacenado en la variable de puntuación. La calificación es la siguiente:

    Más de 50 - D
    Más de 60 - C
    Más de 70 - B
    Más de 80 - A
    Para cualquier otra cosa, se debe mostrar un valor de U.
*/
let puntuación = 60

if(puntuación > 50 && puntuación <= 60) console.log('D') // D
else if(puntuación > 60 && puntuación <= 70) console.log('C')
else if(puntuación > 70 && puntuación <= 80) console.log('B')
else if(puntuación > 80) console.log('A')
else console.log('U')
