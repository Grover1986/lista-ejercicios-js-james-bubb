/*
   Ejercicio 68
   Dada la siguiente sentencia if:
    let dinero = 250;
    if ( ) {
        console.log('Tienes suficiente');
    }
    Complete la expresión booleana dentro de los paréntesis de la instrucción if para mostrar el mensaje en la consola si el valor almacenado dentro de la variable dinero es mayor que 200.
*/
let dinero = 250

if(dinero > 200) {
    console.log('Tienes suficiente');
}