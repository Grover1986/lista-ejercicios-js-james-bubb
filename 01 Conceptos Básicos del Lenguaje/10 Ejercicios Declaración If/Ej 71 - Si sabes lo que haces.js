/*
   Ejercicio 71
    Dada la variable:
    let habilidad = 100
    Escriba una declaración if en JavaScript que muestre la cadena Puedo usar declaraciones if si el valor almacenado en la variable de habilidad es mayor o igual a 100. Intente ajustar el valor almacenado en la variable de habilidad para verificar que su declaración if sea correcta.
*/
let habilidad = 100

if(habilidad >= 100)
    console.log('Puedo usar declaraciones if')