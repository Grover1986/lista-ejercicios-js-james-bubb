/*
   Ejercicio 70
    Dada la siguiente sentencia if:
    let usuario = {
        rol: 'Desarrollador',
        administrador: true,
        cuentaActiva: true
    }
    if ( ) {
        console.log('Tienes acceso');
    }
    Complete la expresión booleana dentro de los paréntesis de la declaración if para mostrar el mensaje en la consola si el objeto de usuario tiene un valor true para su propiedad admin así como un valor true para su propiedad cuentaActiva.
*/
let usuario = {
    rol: 'Desarrollador',
    administrador: true,
    cuentaActiva: true
}

if(usuario.administrador && usuario.cuentaActiva) {
    console.log('Tienes acceso');
}