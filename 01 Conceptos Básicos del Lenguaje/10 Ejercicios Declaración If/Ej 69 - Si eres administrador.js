/*
   Ejercicio 69
    Dada la siguiente sentencia if:
    let usuario = {
        rol: 'Desarrollador',
        administrador: true
    }
    if ( ) {
        console.log('Tienes acceso');
    }
    Complete la expresión booleana dentro de los paréntesis de la instrucción if para mostrar el mensaje en la consola si el objeto de usuario tiene un valor true para su propiedad de administración.
*/
let usuario = {
    rol: 'Desarrollador',
    administrador: true
}

if (usuario.administrador) {
    console.log('Tienes acceso')
}