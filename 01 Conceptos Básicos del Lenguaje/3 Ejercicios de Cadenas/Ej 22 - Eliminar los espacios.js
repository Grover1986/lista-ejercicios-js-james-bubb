/*
    Ejercicio 22
    Elimine el espacio inicial y final de la siguiente cadena:
    const str = 'Mucho espacio inicial y final';
    Puede registrar el resultado en la consola para comprobarlo.
    
    Mis Pasos:
    1. Convertir str en array y guardarlo en variable
    2. Eliminar el espacio inicial y el espacio final del array
    3. Convertir el array a cadena
*/

const str = 'Mucho espacio inicial y final'
let strArray = str.split('')
strArray.splice(5,1)
strArray.splice(22,1)
console.log(strArray.join('')) // 'Muchoespacio inicial yfinal'

    