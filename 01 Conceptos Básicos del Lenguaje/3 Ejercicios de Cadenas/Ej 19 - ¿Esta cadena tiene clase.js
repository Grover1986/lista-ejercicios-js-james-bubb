/*
    Ejercicio 19
   Dada la siguiente cadena:
    const str = '¿Esta cadena tiene clase?';
    Use JavaScript para verificar la existencia de la palabra clase en la cadena original anterior. Puede registrar el resultado (un valor verdadero o falso) en la consola para verificar.
*/
const str = '¿Esta cadena tiene clase?'
console.log(str.includes('clase')) // true
