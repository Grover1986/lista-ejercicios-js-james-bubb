/*
    Ejercicio 17
    Cree dos variables separadas que contengan las cadenas Hello y World, luego muéstrelas juntas como una sola cadena usando console.log.
*/
let saludo1 = 'Hello',
    saludo2 = 'World'
    
console.log(`${saludo1} ${saludo2}`)