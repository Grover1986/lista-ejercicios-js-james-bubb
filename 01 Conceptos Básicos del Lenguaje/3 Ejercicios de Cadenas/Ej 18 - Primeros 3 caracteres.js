/*
    Ejercicio 18
   Dada la siguiente cadena:
    const str = 'El rápido zorro marrón';
    Cree una nueva variable que contenga solo los primeros 3 caracteres de la cadena original anterior. Puede registrar el resultado en la consola para comprobar si es correcto.
*/
const str = 'El rápido zorro marrón'
console.log(str)

const strThreeChars = str.slice(0,3)
console.log(strThreeChars)  // 'El '