/*
    Ejercicio 21
   Dada la siguiente cadena:
    const str = 'Mitad 1 Mitad 2';
    Divida la cadena original en dos partes iguales y almacene cada mitad en su propia variable separada.
*/
let str = 'Mitad 1 Mitad 2'
let strMitad1 = str.slice(0, str.indexOf('',7))
let strMitad2 = str.substring(8)

console.log(strMitad1) // 'Mitad 1'
console.log(strMitad2) // 'Mitad 2'