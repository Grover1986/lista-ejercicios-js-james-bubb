/*
   Ejercicio 24
    Convierta la siguiente cadena para que esté en minúsculas.
    const str = 'El rápido zorro marrón';
*/
const str = 'El rápido zorro marrón'
const strLowerCase = str.toLowerCase()
console.log(strLowerCase)   // 'el rápido zorro marrón'