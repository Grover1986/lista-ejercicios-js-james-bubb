/*
   Ejercicio 25
    Convierta la siguiente cadena para que sea un caso de oración (primera letra de la cadena en mayúscula).
    const str = 'ser o no ser';
*/
const str = 'ser o no ser'
const strFirstLetterUpper = str.charAt(0).toUpperCase().concat(str.substring(1).toLowerCase())
console.log(strFirstLetterUpper)    // 'Ser o no ser'