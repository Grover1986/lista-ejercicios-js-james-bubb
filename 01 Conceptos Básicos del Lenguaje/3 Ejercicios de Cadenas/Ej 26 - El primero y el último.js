/*
   Ejercicio 26
   Dada la siguiente cadena:
    const str = 'esa es la pregunta';
    Use JavaScript para obtener el primer y el último carácter de la cadena original anterior. Registre el resultado en la consola para verificar su solución.
*/
const str = 'esa es la pregunta'
console.log(str.slice(0,1).concat(str.charAt(str.length-1)))    // 'ea'