/*
    Ejercicio 23
   Convierta la siguiente cadena para que esté en mayúsculas.
    const str = 'El rápido zorro marrón';
*/
const str = 'El rápido zorro marrón'
const strUpperCase = str.toUpperCase()
console.log(strUpperCase)   // 'EL RÁPIDO ZORRO MARRÓN'