/*
   Ejercicio 82
    Complete la siguiente declaración de switch:
    let number = 1;
    let cambiar;
    switch(number) {

    }
    Para satisfacer las siguientes condiciones: - Cuando la variable numérica es 1, establezca el valor de la variable de cambiar en 'Activado' - Cuando la variable numérica sea 0, establezca el valor de la variable cambiar en 'Desactivado' - Para cualquier otro valor, establezca la variable de cambiar en 'Desconocido'

    Muestre el contenido de su variable de switch en la consola para verificar sus resultados.
*/
let number = 1
let cambiar

switch(number) {
    case 0:
        cambiar = 'Desactivado'
        console.log(cambiar)
        break
    case 1:
        cambiar = 'Activado'
        console.log(cambiar)
        break
    default:
        cambiar = 'Desconocido'
        console.log(cambiar)
        break
}