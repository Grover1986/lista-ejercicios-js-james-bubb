/*
   Ejercicio 82
    Dada una variable:
    let user = 'admin';
    Escriba una declaración de cambio que muestre un mensaje en la consola según el valor almacenado en la variable user en los siguientes casos: - Si el valor proporcionado es 'admin', muestre Tiene acceso completo - Si el valor proporcionado es 'manager', muestre Usted puede ver los detalles del usuario: para cualquier otro valor, muestre que es un usuario normal

    Intente cambiar el valor de la variable de usuario para probar cada una de las rutas en la declaración de cambio.
*/
let user = 'manager'

switch(user){
    case 'admin':
        console.log('Tiene acceso completo')
        break
    case 'manager':
        console.log('Usted puede ver los detalles del usuario')
        break
    default:
        console.log('Es un usuario normal')
        break
}

