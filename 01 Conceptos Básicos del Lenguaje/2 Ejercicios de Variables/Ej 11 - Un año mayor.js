/*
    Ejercicio 11
   Defina una variable myAge y asígnele un valor numérico que represente la edad que tiene, luego agregue un valor de 1 a la variable. Puede registrar el valor de la variable en la consola para verificar que se haya actualizado.
*/
let myAge = 35
console.log(myAge)

myAge++
console.log(myAge)