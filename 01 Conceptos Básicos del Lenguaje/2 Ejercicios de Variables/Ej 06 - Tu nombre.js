/*
    Ejercicio 06
    Defina una variable llamada myName y asígnele una cadena que contenga su nombre completo. Considere qué tipo de palabra clave de declaración va a utilizar, p. ¿Cambiará su nombre durante el curso de un programa de JavaScript?
*/
let myName = 'Grover Cristobal'
console.log(myName)

myName = 'Elvis Cristobal'
console.log(myName)