/*
    Ejercicio 15
  Defina dos variables, firstName y lastName y almacene su nombre y apellido en ellas respectivamente. Luego, cree una tercera variable fullName que use las variables firstName y lastName para crear su nombre completo.
*/
let firstName = 'Elvis Grover', 
    lastName = 'Cristobal Garrido',
    fullName = `${firstName} ${lastName}`

console.log(fullName)