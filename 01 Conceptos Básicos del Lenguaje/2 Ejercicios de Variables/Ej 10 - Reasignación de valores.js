/*
    Ejercicio 10
    Defina una variable llamada comida favorita y asígnele un valor de cadena (de su comida favorita en particular). Use console.log para mostrarlo en la consola y luego vuelva a asignar un nuevo valor a la variable favouriteFood. Registre el nuevo valor en la consola para verificar que haya cambiado.
*/
let favouriteFood = 'Estofado de Pollo'
console.log(favouriteFood)

favouriteFood = 'Ceviche'
console.log(favouriteFood)