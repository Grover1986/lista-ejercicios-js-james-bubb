/*
   Ejercicio 53
    Usando la siguiente matriz:
    let languages ​​= ['JavaScript', 'PHP', 'C#', 'Python', 'Scala', 'Perl'];
    Cree una nueva variable firstLanguage que contenga el primer elemento de la matriz de idiomas. Su código también debería eliminar el primer elemento de la matriz de idiomas.
*/
let languages = ['JavaScript', 'PHP', 'C#', 'Python', 'Scala', 'Perl']
let firstLanguage = languages.at(0)
let removeFirstElement = languages.shift()

console.log(firstLanguage)  // JavaScript
console.log(removeFirstElement) // JavaScript
console.log(languages)  // ['PHP', 'C#', 'Python', 'Scala', 'Perl']