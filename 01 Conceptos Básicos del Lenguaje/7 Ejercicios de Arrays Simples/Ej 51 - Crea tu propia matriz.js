/*
   Ejercicio 51
   Dado el siguiente conjunto de variables de JavaScript:
    const item1 = 'Manzana';
    const item2 = 'Naranja';
    const item3 = 'Plátano';
    const item4 = 'Kiwi';
    const item5 = 'Mango';
    Convierta el código anterior para que todos los valores almacenados en las variables sean parte de una matriz (es decir, reescriba el código para usar una matriz en lugar de variables individuales para cada fruta).
*/
const item1 = 'Manzana'
const item2 = 'Naranja'
const item3 = 'Plátano'
const item4 = 'Kiwi'
const item5 = 'Mango'

const frutas = [item1, item2, item3, item4, item5]
console.log(frutas) // ['Manzana','Naranja','Plátano','Kiwi','Mango']