/*
   Ejercicio 48
    Dada la siguiente matriz:
    const arr = [1, 'Sarah', 5, 'Hamza', { a: 1 } , { b: 2 }, 10];
    Determine la longitud de la matriz y registre el resultado en la consola.
*/
const arr = [1, 'Sarah', 5, 'Hamza', { a: 1 } , { b: 2 }, 10]
console.log(arr.length) // 7