/*
   Ejercicio 50
   Dada la siguiente matriz:
    const arr = [92, 87, 45, 80, 1, 94, 20]
    Defina una variable llamada tercero que seleccione el tercer elemento de la matriz. Compruebe que tiene el valor correcto mostrándolo en la consola.
*/
const arr = [92, 87, 45, 80, 1, 94, 20]
const tercero = arr.at(2)
console.log(tercero)    // 45