/*
   Ejercicio 55
    Usando la siguiente matriz:
    let languages ​​= ['JavaScript', 'PHP', 'C#', 'Python', 'Scala', 'Perl'];
    Agregue el idioma Node.js al comienzo de la matriz de idiomas, convirtiéndolo en el primer elemento de la matriz.
*/
let languages = ['JavaScript', 'PHP', 'C#', 'Python', 'Scala', 'Perl']
let addFirstLanguage = languages.unshift('Node.js')
console.log(languages)  // ['Node.js', 'JavaScript', 'PHP', 'C#', 'Python', 'Scala', 'Perl']