/*
   Ejercicio 52
    Usando la siguiente matriz:
    let languages ​​= ['JavaScript', 'PHP', 'C#', 'Python', 'Scala', 'Perl'];
    Cree una nueva variable lastLanguage que contenga el último elemento de la matriz de idiomas. Su código también debería eliminar el último elemento de la matriz de idiomas.
*/
let languages = ['JavaScript', 'PHP', 'C#', 'Python', 'Scala', 'Perl']
let lastLanguage = languages.at(languages.length-1)
let removeLastElement = languages.pop()

console.log(lastLanguage)   // Perl
console.log(removeLastElement)  // Perl
console.log(languages)  // ['JavaScript', 'PHP', 'C#', 'Python', 'Scala']

