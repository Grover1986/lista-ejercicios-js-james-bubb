/*
   Ejercicio 54
    Usando la siguiente matriz:
    let languages ​​= ['JavaScript', 'PHP', 'C#', 'Python', 'Scala', 'Perl'];
    Agregue el idioma Ruby al final de la matriz de idiomas, convirtiéndolo en el último elemento de la matriz.
*/
let languages = ['JavaScript', 'PHP', 'C#', 'Python', 'Scala', 'Perl']
let addLastLanguage = languages.push('Ruby')
console.log(languages)  // ['JavaScript', 'PHP', 'C#', 'Python', 'Scala', 'Perl', 'Ruby']