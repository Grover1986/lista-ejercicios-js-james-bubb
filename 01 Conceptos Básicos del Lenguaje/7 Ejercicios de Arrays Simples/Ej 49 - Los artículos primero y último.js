/*
   Ejercicio 49
    Dada la siguiente matriz:
    const arr = [102, 4, 22, 7, 32, 9, 40];
    Defina dos variables llamadas first y last que den a seleccionar el primer y el último valor de la matriz. Verifique que sus variables tengan los valores de 102 y 40 respectivamente registrando el resultado en la consola.
*/
const arr = [102, 4, 22, 7, 32, 9, 40]
const first = arr.at(0)
const last = arr.at(arr.length-1)
console.log(first)  // 102
console.log(last)   // 40