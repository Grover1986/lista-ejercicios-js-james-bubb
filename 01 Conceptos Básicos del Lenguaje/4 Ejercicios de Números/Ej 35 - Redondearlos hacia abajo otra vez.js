/*
   Ejercicio 35
    Dado el siguiente número:
    const num = 3.5
    Use JavaScript para redondear el número a 3 y mostrar el resultado en la consola.
*/
const num = 3.5
console.log(Math.floor(num))   // 3