/*
   Ejercicio 31
    Dado el siguiente número:
    const num = 23.001293;
    Muestre el número en la consola con solo 2 decimales mostrados.
*/
const num = 23.001293
console.log(num.toFixed(2)) // 23.00