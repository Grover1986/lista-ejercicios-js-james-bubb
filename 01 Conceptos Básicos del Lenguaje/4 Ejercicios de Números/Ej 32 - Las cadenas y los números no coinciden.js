/*
   Ejercicio 32
    Dadas las siguientes variables:
    const a = 2
    const b = '2'
    Use JavaScript para sumar las dos variables para lograr el resultado de 4. Puede mostrar su resultado en la consola para verificar.
*/
const a = 2
const b = '2'

const convertStrToNum = Math.trunc(b) // con trunc() convertimos un número entre comillas (string) a un number
console.log(a + convertStrToNum) // 4