/*
   Ejercicio 43
    Dado el siguiente fragmento de código:
    const firstName = 'Jennie';
    const secondName = 'Harper';
    const str = 'Buenas tardes' + firstName + ' ' + secondName ', ¡encantado de conocerte!';
    Vuelva a escribir el código anterior para usar un literal de plantilla. Puede comprobar su código registrando el nuevo literal de plantilla en la consola.
*/
const firstName = 'Jennie'
const secondName = 'Harper'
const str = `Buenas tardes ${firstName} ${secondName}, ¡encantado de conocerte!`
console.log(str) // 'Buenas tardes Jennie Harper, ¡encantado de conocerte!' 