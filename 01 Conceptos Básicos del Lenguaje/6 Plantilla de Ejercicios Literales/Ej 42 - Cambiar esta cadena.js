/*
   Ejercicio 42
   Dado el siguiente fragmento de código:
    const nombre = 'Sofía';
    const str = 'Buenas tardes' + nombre + '¡encantado de conocerte!';
    Vuelva a escribir el código anterior para usar un literal de plantilla. Puede comprobar su código registrando el nuevo literal de plantilla en la consola.
*/
const nombre = 'Sofía'
const str = `Buenas tardes ${nombre} ¡encantado de conocerte!`

console.log(str)  // 'Buenas tardes Sofía ¡encantado de conocerte!'