/*
   Ejercicio 44
    Dado el siguiente fragmento de código:
    const str = 'El veloz zorro marrón\n salta sobre\n el perro perezoso';
    Vuelva a escribir el código anterior para usar un literal de plantilla. Puede comprobar su código registrando el nuevo literal de plantilla en la consola.
*/
const str = `El veloz zorro $ marrón\n salta sobre\n el perro perezoso`
console.log(str)