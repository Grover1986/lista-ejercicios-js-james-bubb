/*
   Ejercicio 46
    Dado el siguiente fragmento de código:
    const error = new Error('Error desconocido');
    const str = 'Ocurrió un error: ' + error.message;
    Vuelva a escribir el código anterior para usar un literal de plantilla. Puede comprobar su código registrando el nuevo literal de plantilla en la consola.
*/
const error = new Error('Error desconocido');
const str = 'Ocurrió un error: ' + error.message;

const resultado = `Ocurrió un error: ${error.message}`
console.log(resultado)