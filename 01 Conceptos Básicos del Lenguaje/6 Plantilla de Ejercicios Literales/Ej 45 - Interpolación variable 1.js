/*
   Ejercicio 45
   Dado el siguiente fragmento de código:
    const edad = 25;
    const cumpleaños = '26/08/1995';
    const str = 'Tengo ' + edad + ' años en mi cumpleaños (' + cumpleaños + ')';
    Vuelva a escribir el código anterior para usar un literal de plantilla. Puede comprobar su código registrando el nuevo literal de plantilla en la consola.
*/
const edad = 25
const cumpleaños = '26/08/1995'
const str = 'Tengo ' + edad + ' años en mi cumpleaños (' + cumpleaños + ')'

const resultado = `Tengo ${edad} años en mi cumpleaños ${(cumpleaños)}`
console.log(str)