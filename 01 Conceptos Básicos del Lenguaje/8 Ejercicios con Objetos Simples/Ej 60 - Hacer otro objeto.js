/*
   Ejercicio 60
    Dados los siguientes dos conjuntos de variables:
    
    *Propiedades del objeto
    const nombre = 'Ricardo Smith';
    const edad = 19;
    const rol = 'Desarrollador';

    *Propiedades del amigo
    const nombreAmigo = 'Robin Marshall';
    const edadAmigo = 20;
    const rolAmigo = 'Desarrollador';
    Cree un objeto que tenga las propiedades bajo el encabezado "Propiedades del objeto" y guárdelo en una variable persona. Luego, cree una propiedad adicional en el objeto persona llamada amigo. La propiedad de amigo debe contener otro objeto que tenga las propiedades en la sección "Propiedades de amigo".
*/
const persona = {
    encabezado: 'Propiedades de Objeto',
    nombre: 'Ricardo Smith',
    edad: 19,
    rol: 'Desarrollador',
    amigo: {
        encabezado: 'Propiedades de Amigo',
        nombreAmigo: 'Robin Marshall',
        edadAmigo: 20,
        rolAmigo: 'Desarrollador'
    }
}