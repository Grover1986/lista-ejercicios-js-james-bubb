/*
   Ejercicio 58
    Dado el siguiente Objeto que representa a una persona:
    const persona = {
        nombre: 'James',
        edad: 21,
        rol: 'Desarrollador',
        idiomas: ['francés', 'inglés', 'alemán', 'polaco', 'italiano']
    };
    Con JavaScript, determine cuántos idiomas ha enumerado la persona en el objeto (la longitud de la propiedad idiomas).
*/
const persona = {
    nombre: 'James',
    edad: 21,
    rol: 'Desarrollador',
    idiomas: ['francés', 'inglés', 'alemán', 'polaco', 'italiano']
}
console.log(persona.idiomas.length) // 5