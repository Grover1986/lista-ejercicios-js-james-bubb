/*
   Ejercicio 59
    Dadas las siguientes variables:
    const nombre = 'Willie';
    const apellido = 'Phillips';
    const edad = 26;
    const rol = 'Desarrollador';
    Cree un objeto que tenga los nombres de las variables anteriores como propiedades del objeto y valores que correspondan a los valores almacenados en cada variable.
*/
const persona = {
    nombre: 'Willie',
    apellido: 'Phillips',
    edad: 26,
    rol: 'Desarrollador',
}