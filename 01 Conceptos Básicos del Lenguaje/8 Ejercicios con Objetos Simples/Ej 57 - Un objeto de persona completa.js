/*
   Ejercicio 57
    Dado el siguiente Objeto que representa a una persona:
    const persona = {
        nombre: 'Sarah',
        apellido: 'Hennings',
        edad: 33,
        rol: 'Desarrollador'
    };
    Escriba una declaración en consola que registre el nombre completo de la persona (nombre y apellido) en la salida de la consola. Debe incluir un espacio entre su nombre y apellido en su salida.
*/
const persona = {
    nombre: 'Sarah',
    apellido: 'Hennings',
    edad: 33,
    rol: 'Desarrollador'
}
console.log(`${persona.nombre} ${persona.apellido}`)  // 'Sarah Hennings'