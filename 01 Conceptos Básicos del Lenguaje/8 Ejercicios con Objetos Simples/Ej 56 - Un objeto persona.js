/*
   Ejercicio 56
    Dado el siguiente Objeto que representa a una persona:
    const persona = {
        nombre: 'Hamza',
        edad: 21,
        rol: 'Desarrollador'
    };
    Acceda a la propiedad nombre de la variable persona y visualice el contenido en la consola.
*/
const persona = {
    nombre: 'Hamza',
    edad: 21,
    rol: 'Desarrollador'
}
console.log(persona.nombre) // Hamza