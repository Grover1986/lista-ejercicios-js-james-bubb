/*
   Ejercicio 41
   Almacene el valor booleano de verdadero en una variable llamada myBoolean y muestre el valor inverso (falso) de la variable cuando lo muestre en la consola.
*/
const myBoolean = true
console.log(!myBoolean)