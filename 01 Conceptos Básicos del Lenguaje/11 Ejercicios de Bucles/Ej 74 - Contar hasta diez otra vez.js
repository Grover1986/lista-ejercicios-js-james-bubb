/*
   Ejercicio 74
    Escriba un bucle while en JavaScript que cuente del 1 al 10 y muestre estos números en la consola.
*/
let i = 1

while(i <= 10){
    console.log(i)
    i++
}