/*
   Ejercicio 75
    Usando cualquier tipo de ciclo, cuente hacia atrás de 10 a 1 con JavaScript y muestre estos números en la consola.
*/
let i = 10

while(i >= 1){
    console.log(i)
    i-=1
}