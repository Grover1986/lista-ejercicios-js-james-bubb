/*
   Ejercicio 76
    Usando cualquier tipo de bucle, cuente desde el valor de 1 a 25 sumando cada número para formar un total (por ejemplo, 1 + 2 + 3 + 4...) y luego muestre este valor en la consola.
*/
let suma = 0
let cont = 1

// Modo 1
while(cont <= 25){
    suma = suma + cont
    console.log(suma)
    cont = cont + 1
}

// sólo q se tendría q elegir uno de los dos modos

// Modo 2 
for(cont=1; cont<=25; cont++){
    suma = suma + cont
    console.log(suma)
}