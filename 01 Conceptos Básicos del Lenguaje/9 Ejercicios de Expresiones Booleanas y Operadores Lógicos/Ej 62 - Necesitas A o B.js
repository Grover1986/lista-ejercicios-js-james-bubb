/*
   Ejercicio 62
    Dadas las dos variables:
    let ​​A = false
    let ​​B = true
    Utilice un operador lógico de JavaScript para crear una expresión booleana que dé como resultado verdadero si A y B son verdaderos.
*/
let A = false
let B = true

console.log(A || B) // true