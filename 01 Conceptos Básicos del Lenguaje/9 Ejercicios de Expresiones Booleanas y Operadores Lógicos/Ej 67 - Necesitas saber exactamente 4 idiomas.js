/*
   Ejercicio 67
    Dada la matriz:
    let languages ​​= ['Inglés', 'Francés', 'Italiano', 'Español'];
    Escriba una expresión booleana (que dará como resultado verdadero) para determinar si la matriz de idiomas tiene exactamente 4 elementos. Llame a la función .pop() o .push() en la matriz para eliminar o agregar un elemento y luego verifique su expresión nuevamente.
*/
let languages = ['Inglés', 'Francés', 'Italiano', 'Español']
console.log(languages.length == 4)  // true
console.log(languages.push('Hebreo'))   // 5
console.log(languages.length == 4) // false