/*
   Ejercicio 63
    Dadas las tres variables:
    let ​​A = false;
    let ​​B = true;
    let ​​C = false;
    Use un operador lógico de JavaScript para crear una expresión booleana que resulte verdadera cuando alguna de las variables sea verdadera.
*/
let A = false
let B = true
let C = false

console.log(A || B || C)  // true