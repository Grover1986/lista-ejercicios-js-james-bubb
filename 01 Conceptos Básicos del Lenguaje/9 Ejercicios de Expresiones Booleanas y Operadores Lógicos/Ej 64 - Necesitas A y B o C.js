/*
   Ejercicio 64
    Dadas las tres variables:
    let ​​A = false
    let ​​B = false
    let ​​C = true
    Utilice un operador lógico de JavaScript para crear una expresión booleana que resulte verdadera cuando el valor de A y B sea verdadero o el valor de C sea verdadero.
*/
let A = false
let B = false
let C = true

console.log((A && B) || C)  // true