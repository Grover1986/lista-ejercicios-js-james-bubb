/*
   Ejercicio 66
    Dada la matriz:
    let languages ​​= ['Inglés', 'Francés', 'Italiano'];
    Escriba una expresión booleana (que dará como resultado verdadero) para determinar si la matriz de idiomas tiene al menos 3 elementos. Llame a la función .pop() en la matriz para eliminar un elemento y luego verifique su expresión nuevamente.
*/
let languages = ['Inglés', 'Francés', 'Italiano']
console.log(languages.length > 2)   // true
console.log(languages.pop())   // eliminamos Italiano
console.log(languages.length > 2)   // false