/*
   Ejercicio 61
    Dadas las dos variables:
    let ​​A = true;
    let ​​B = true;
    Utilice un operador lógico de JavaScript para crear una expresión booleana que solo resulte verdadera cuando tanto A como B sean verdaderas.
*/
let A = true
let B = true

console.log(A && B) // true