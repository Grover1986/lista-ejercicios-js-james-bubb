/*
   Ejercicio 65
    Dada la variable:
    let ​​edad = 20
    Escriba una expresión booleana (que dará como resultado falso) para determinar si el valor almacenado en la variable edad es al menos 21. Verifique que la expresión sea correcta cambiando el valor almacenado en edad a 21 y 22.
*/
let edad = 20
console.log(edad > 20)
edad = 21
console.log(edad > 20)
edad = 22
console.log(edad > 20)