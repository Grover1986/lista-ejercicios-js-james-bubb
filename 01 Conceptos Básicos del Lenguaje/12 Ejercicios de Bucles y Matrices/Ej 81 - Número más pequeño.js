/*
   Ejercicio 81
    Dada la siguiente matriz:
    const números = [33, 42, 72, 91, 52, 98, 22, 19, 7];
    Use un bucle for o while de JavaScript para determinar el número más pequeño en la matriz de números y mostrarlo en la consola.
*/
const numeros = [33, 42, 72, 91, 52, 98, 22, 19, 7]
let menor = numeros[0]

for(let i=0; i<numeros.length; i++){
    if(numeros[i] < menor){
        menor = numeros[i]
    }
}
console.log(menor)  // 7