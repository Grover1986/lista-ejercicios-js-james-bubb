/*
   Ejercicio 77
    Dada la siguiente matriz:
    const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    Use un bucle for o while de JavaScript para sumar los valores que se almacenan en la matriz de números. Muestre su resultado en la consola.
*/
const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9]

let acum = 0
let i = 0

while(i < numbers.length){
    acum = acum + numbers[i]
    console.log(acum) // 45
    i = i + 1
}