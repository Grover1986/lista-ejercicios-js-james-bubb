/*
   Ejercicio 80
    Dada la siguiente matriz:
    const numbers = [33, 42, 72, 91, 52, 98, 22, 19, 7];
    Use un bucle for o while de JavaScript para determinar el número más grande en la matriz de números y mostrarlo en la consola.
*/
const numbers = [33, 42, 72, 91, 52, 98, 22, 19, 7]
let mayor = 0 // creamos la variable de salida 'mayor'

for(let i=0; i<numbers.length; i++){
    if(numbers[i] > mayor){ // va comparando cada valor del array con el valor q se va guardando en variable 'mayor'
        mayor = numbers[i] // aquí se va guardando el valor mayor
    }
}
console.log(mayor) // resultado 98, y fuera del bucle for, xq sino genera varios valores
  
