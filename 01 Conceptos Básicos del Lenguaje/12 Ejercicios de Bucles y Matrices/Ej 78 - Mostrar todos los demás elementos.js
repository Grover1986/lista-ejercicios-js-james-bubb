/*
   Ejercicio 78
    Dada la siguiente matriz:
    const fruit = ['Cereza', 'Manzana', 'Plátano', 'Naranja', 'Kiwi', 'Piña'];
    Use un bucle for o while de JavaScript para mostrar solo todos los demás elementos en la matriz de frutas en la consola. (La salida debe ser 'Manzana', 'Naranja', 'Piña');
*/
const fruit = ['Cereza', 'Manzana', 'Plátano', 'Naranja', 'Kiwi', 'Piña']
let i = 0

while(i < fruit.length){
    if(i % 2 == 1){
        console.log(fruit[i])  // Manzana, Naranja, Piña
    }
    i++
}