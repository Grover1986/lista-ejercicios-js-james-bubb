/*
   Ejercicio 79
    Dada la siguiente matriz:
    const numbers = [1, 6, 2, 6, 3, 6, 6, 4, 6, 5, 6]
    Use un bucle for o while de JavaScript para contar el número de seises (valor de '6') que están en la matriz de numbers. Muestra tu resultado en la consola.
*/
const numbers = [1, 6, 2, 6, 3, 6, 6, 4, 6, 5, 6]
let cont = 0

for(let i = 0; i < numbers.length; i++){
    if(numbers[i] === 6){
        cont = cont + 1
        console.log(cont)
    }
}